/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.linkedlistimplementation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author andra
 */
@ExtendWith(MockitoExtension.class)
public class BagTest<E> {

    private Bag<E> underTest;
    private E element;
    private static final int EMPTY_SIZE = 0;

    @BeforeEach
    void init() {
        underTest = new Bag<>();
        element = (E) new Object();
    }

    @Test
    public void testGetSizeWhenBagIsEmpty() {
        //Given

        //When
        int size = underTest.getSize();

        //Then
        assertEquals(EMPTY_SIZE, size);

    }

    //first is null && size is 0
    @Test
    public void testIsEmptyWhenBagIsEmpty() {
        //Given

        //When
        boolean isEmpty = underTest.isEmpty();
        //Then
        assertTrue(isEmpty);
    }

    //first is not null && size is not 0
    @Test
    public void testIsEmptyWhenBagIsNotEmpty() {
        //Given
        underTest.add(element);
        //When
        boolean isEmpty = underTest.isEmpty();
        //Then
        assertFalse(isEmpty);
    }

    //first is null && size is not 0
    @Test
    public void testIsEmptyWhenBag1stElementIsNullSizeIs1() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
        //Given
        Class type = underTest.getClass();
        Object o = type.getDeclaredConstructor().newInstance();
        Field bagSize = type.getDeclaredField("size");
        bagSize.setAccessible(true);
        underTest = (Bag<E>) o;
        bagSize.set(o, 1);

        //When
        boolean isEmpty = underTest.isEmpty();

        //Then
        assertFalse(isEmpty);
    }

    //first is null && size is 1
    @Test
    public void testIsEmptyWhenBag1stElementIsNullAndSizeIs1() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
        //Given
        Class type = underTest.getClass();
        Object o = type.getDeclaredConstructor().newInstance();
        Field bagSize = type.getDeclaredField("size");
        bagSize.setAccessible(true);
        underTest = (Bag<E>) o;
        bagSize.set(o, 1);

        //When
        boolean isEmpty = underTest.isEmpty();

        //Then
        assertFalse(isEmpty);
    }

    //first is null && size is 1
    @Test
    public void testAddWhenBag1stElementIsNullAndSizeIs1() throws NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchFieldException {
        //Given
        Class type = underTest.getClass();
        Object o = type.getDeclaredConstructor().newInstance();
        Field bagSize = type.getDeclaredField("size");
        bagSize.setAccessible(true);
        underTest = (Bag<E>) o;
        bagSize.set(o, 1);

        //When
        boolean isAddedToBag = underTest.add((E) new ArrayList<>());

        //Then
        assertFalse(isAddedToBag);
    }

    @Test
    public void testGetSizeWhenBagHas1Element() {
        //Given
        underTest.add(element);

        //When
        int size = underTest.getSize();

        //Then
        assertEquals(EMPTY_SIZE + 1, size);
    }

    @Test
    public void testGetSizeWhenBagHas2Elements() {
        //Given
        underTest.add(element);
        underTest.add(element);

        //When
        int size = underTest.getSize();

        //Then
        assertEquals(EMPTY_SIZE + 2, size);

    }

    @Test
    public void shouldReturnFalseWhenAddingNull() {
        //Given

        //When
        boolean addedToBag = underTest.add(null);

        //Then
        assertFalse(addedToBag);
    }

    @Test
    public void shouldReturnTrueWhenAdding1Number() {
        //Given

        //When
        boolean addedToBag = underTest.add((E) Integer.valueOf(1));

        //Then
        assertTrue(addedToBag);

    }

    @Test
    public void shouldReturnTrueWhenAdding2Numbers() {
        //Given

        //When
        boolean bothAddedToBag = underTest.add((E) Double.valueOf(1.0454)) && underTest.add((E) Double.valueOf(2.05));

        //Then
        assertTrue(bothAddedToBag);
    }

    @Test
    public void shouldReturnTrueWhenAdding1String() {
        //Given

        //When
        boolean addedToBag = underTest.add((E) "Element");

        //Then
        assertTrue(addedToBag);
    }

    @Test
    public void shouldReturnTrueWhenAdding2Strings() {
        //Given

        //When
        boolean bothAddedToBag = underTest.add((E) "Element_1") && underTest.add((E) "Element_2");

        //Then
        assertTrue(bothAddedToBag);
    }

    @Test
    public void shouldThrowClassCastExceptionWhenAddingNumberAndString() {
        //Given
        element = (E) Integer.valueOf(1);
        //When
        underTest.add(element);

        //Then
        assertThrows(ClassCastException.class, () -> underTest.add((E) "Element"));

    }

    @Test
    public void testRemove() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        boolean isRemoved = underTest.remove(Integer.valueOf(2));
        boolean containsRemoved = underTest.contains(Integer.valueOf(2));
        List<Boolean> manualContainsCheck = new ArrayList<>();
        underTest.forEach((a) -> {
            if (a.equals((E) Integer.valueOf(2))) {
                manualContainsCheck.add(true);
            }
        });
        //Then
        assertAll(
                () -> assertTrue(isRemoved),
                () -> assertFalse(containsRemoved),
                () -> assertFalse(manualContainsCheck.contains(Boolean.TRUE))
        );

    }

    @Test
    public void testRemoveWhenBagIsEmpty() {
        //Given

        //When
        boolean isRemoved = underTest.remove(Integer.valueOf(2));

        //Then
        assertFalse(isRemoved);
    }

    @Test
    public void testRemoveNullFromBag() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        boolean isRemoved = underTest.remove(null);

        //Then
        assertFalse(isRemoved);
    }

    @Test
    public void testRemoveDifferentTypeFromBag() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        boolean isRemoved = underTest.remove("Element");
        //Then
        assertFalse(isRemoved);
    }

    @Test
    public void testBagIteratorNextWhenHasNextIsFalse() {
        //Given

        //When
        //Then
        assertThrows(NoSuchElementException.class, () -> underTest.iterator().next());

    }

    @Test
    public void testContainsWhenElementIsContained() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        boolean contained = underTest.contains(2);
        //Then
        assertTrue(contained);
    }

    @Test
    public void testContainsWhenElementIsNotContained() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        boolean contained = underTest.contains(8);
        //Then
        assertFalse(contained);
    }

    @Test
    public void testContainsWhenElementIsDifferentType() {
        //Given
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(2));
        underTest.add((E) Integer.valueOf(1));
        underTest.add((E) Integer.valueOf(3));

        //When
        Executable checkIfContains = () -> underTest.contains(3.03d);
        //Then
        assertThrows(NoSuchElementException.class, checkIfContains);
    }
}
