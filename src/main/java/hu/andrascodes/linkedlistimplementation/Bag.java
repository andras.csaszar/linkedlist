/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.andrascodes.linkedlistimplementation;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;

/**
 *
 * @author andra
 */
public class Bag<E> implements Collection<E> {

    private int size = 0;
    private Node<E> first;
    private Node<E> last;

    private class Node<E> {

        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;

        }

        @Override
        public String toString() {
            String previous = prev == null ? "null" : prev.item.toString();
            String nextE = next == null ? "null" : next.item.toString();

            return "Node{" + "item: " + this.item + "}"
                    + " prev: " + previous + ", "
                    + " next: " + nextE;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 73 * hash + Objects.hashCode(this.item);
            hash = 73 * hash + Objects.hashCode(this.next);
            hash = 73 * hash + Objects.hashCode(this.prev);
            return hash;
        }

        public boolean hasSameItem(Node<E> otherNode) {
            return this.item.equals(otherNode.item);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final Node<?> other = (Node<?>) obj;
            if (!Objects.equals(this.item, other.item)) {
                return false;
            }
            if (!Objects.equals(this.next, other.next)) {
                return false;
            }
            if (!Objects.equals(this.prev, other.prev)) {
                return false;
            }
            return true;
        }

    }

    private void linkFirst(E element) {
        first = new Node<>(null, element, null);
        last = first;
        increaseSizeByOne();
    }

    private void unlinkNode(Node<E> nodeToUnlink) {
        if (nodeToUnlink == null) {
            throw new NoSuchElementException("provided node is null");
        }
        if (nodeToUnlink.equals(first)) {
            unlinkFirstNode();
        } else if (nodeToUnlink.equals(last)) {
            unlinkLastNode();
        } else {
            unlinkMidNode(nodeToUnlink);
        }
        decreaseSizeByOne();

    }

    private void unlinkFirstNode() {
        first.next.prev = null;
        setFirst(first.next);
    }

    private void unlinkLastNode() {
        //last.prev.next = null;
        Node<E> succeeding = last.prev;
        succeeding.next = last.next;
        succeeding.prev = last.prev;
        setLast(succeeding);

    }

    private void unlinkMidNode(Node<E> nodeToUnlink) {

        (nodeToUnlink.next).prev = nodeToUnlink.prev;
        (nodeToUnlink.prev).next = nodeToUnlink.next;

    }

    public void addToFront(E element) {
        insertToFront(element);
        increaseSizeByOne();
    }

    public void addToEnd(E element) {
        insertToEnd(element);
        increaseSizeByOne();
    }

    private void increaseSizeByOne() {
        size++;
    }

    private void decreaseSizeByOne() {
        size--;
    }

    private void insertToBeforeXThElement() {

    }

    public E getXthElement(int x) {
        if (x < 0 || x > size - 1) {
            throw new NoSuchElementException();
        }

        Iterator<E> iter = this.iterator();
        int position = 0;
        Node<E> currentNode = first;

        while (iter.hasNext() && position != x) {
            iter.next();
            position++;
            currentNode = currentNode.next;
        }
        return currentNode.item;
    }

    private void insertToFront(E element) {
        linkElementBeforeNode(element, first);
    }

    private void insertToEnd(E element) {
        linkLast(linkElementAfterNode(element, last));

    }

    private void linkLast(Node<E> toBeLast) {
        last = toBeLast;
    }

    private Node<E> linkElementBeforeNode(E element, Node<E> toBeNext) {
        Node<E> linkedElementsNode = new Node<E>(
                toBeNext.prev,
                element,
                toBeNext
        );
        toBeNext.prev = linkedElementsNode;
        return linkedElementsNode;
    }

    private Node<E> linkElementAfterNode(E element, Node<E> toBePrevious) {
        Node<E> linkedElementsNode = new Node<E>(
                toBePrevious,
                element,
                toBePrevious.next
        );
        toBePrevious.next = linkedElementsNode;
        return linkedElementsNode;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return first == null && size == 0;
    }

    @Override
    public boolean contains(Object element) {

        if (this.first.item.getClass() != element.getClass()) {
            throw new NoSuchElementException("Contained elements are of different type than received Object.");
        } else {
            try {
                findFirst((E) element);
                return true;
            } catch (NoSuchElementException ex) {
                return false;
            }
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new BagIterator<E>();
    }

    @Override
    public boolean remove(Object element) {
        E elementToRemove = (E) element;
        return removeFirstMatch(elementToRemove);
    }

    private class BagIterator<E> implements Iterator<E> {

        private Node<E> pointerNode = new Node<E>(null, null, (Node<E>) first);

        @Override
        public boolean hasNext() {
            return pointerNode.next != null;

        }

        @Override
        public E next() {
            if (hasNext()) {
                pointerNode = pointerNode.next;
                return pointerNode.item;
            } else {
                throw new NoSuchElementException();
            }

        }

        public boolean hasPrevious() {
            return pointerNode.prev != null;
        }

        public E previous() {
            if (hasPrevious()) {
                pointerNode = pointerNode.prev;
                return pointerNode.item;
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public void remove() {
            unlinkNode((Node) pointerNode);
        }
    }

    @Override
    public Object[] toArray() {

        Object[] array = new Object[size];
        int counter = 0;
        for (E element : this) {
            array[counter] = element;
            counter++;
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] givenArray) {
        if (givenArray == null) {
            throw new NullPointerException("Given array was null.");
        }

        if (givenArray.length == 0 || givenArray.length < this.size) {
            T[] array = (T[]) this.toArray();
            return array;
        }

        if (givenArray.length >= this.size) {
            int counter = 0;
            for (E element : this) {
                givenArray[counter] = (T) element;
                counter++;
            }
            if (givenArray.length > this.size) {
                givenArray[counter] = null;
            }
            return givenArray;

        }
//
        return givenArray;
    }

    @Override
    public boolean add(E element
    ) {
        if (element == null) {
            return false;
        }
        if (isEmpty()) {
            linkFirst(element);

        } else if (first == null) {
            return false;
        } else {
            if (first.item.getClass() != element.getClass()) {
                throw new ClassCastException("Element wanted to be added is incompatible type to Bag.");
            }

            addToEnd(element);
        }
        return true;
    }

    private Node<E> findFirst(E element) throws NoSuchElementException {
        Node<E> pointer = first;
        while (pointer.next != null) {
            if (pointer.item.equals(element)) {
                return pointer;
            }
            pointer = pointer.next;
        }
        throw new NoSuchElementException("no such element");

    }

    public boolean removeFirstMatch(E element) {
        E toRemove;
        if (element == null || isEmpty()) {
            return false;
        } else if (this.getFirst().item.getClass() != element.getClass()) {
            return false;
        } else {
            toRemove = (E) element;

            Iterator<E> it = this.iterator();
            boolean foundMatch = false;

            while (it.hasNext() && !foundMatch) {

                E currentElement = it.next();
                if (currentElement.equals(toRemove)) {
                    unlinkNode(findFirst(element));
                    foundMatch = true;
                }
            }
            return foundMatch;
        }
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        boolean allIn = true;
        for (Object item : collection) {
            if (!contains(item)) {
                allIn = false;
            }
        }
        return allIn;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        boolean changed = false;

        for (E item : collection) {
            changed = changed || add(item);
        }
        return changed;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean changed = false;
        for (Object item : collection) {
            if (contains(item)) {
                changed = changed || remove(item);
            }
        }
        return changed;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean changed = false;
        for (E element : this) {
            if (!collection.contains(element)) {
                changed = changed || remove(element);
            }
        }
        return changed;
    }

    @Override
    public void clear() {
        removeAll(this);
    }

    @Override
    public void forEach(Consumer<? super E> action) {
        Collection.super.forEach(action); //To change body of generated methods, choose Tools | Templates.
    }

    public int getSize() {
        return size;
    }

    public E getFirstItem() {
        return getFirst().item;
    }

    public E getLastItem() {
        return getLast().item;
    }

    public E getFirstMatchNextItem(E element) {
        return findFirst(element).next.item;
    }

    public E getFirstMatchPreviousItem(E element) {
        return findFirst(element).prev.item;
    }

    private Node<E> getFirst() {
        return first;
    }

    private void setFirst(Node<E> first) {
        this.first = first;
    }

    private Node<E> getLast() {
        return last;
    }

    private void setLast(Node<E> last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "BagALinkedListImpl{" + "size=" + getSize()
                + ", first=" + getFirst().item
                + ", last=" + getLast().item
                + "}";

    }

}
